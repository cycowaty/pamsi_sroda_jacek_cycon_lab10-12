#pragma once

class kolkokrzyzyk
{
	char t[10],gracz,wybor;
    int licznik;
public:
	kolkokrzyzyk(void);

	void rysuj_plansze();
	void wyzeruj_plansze();
	bool czy_wygrana(char g, bool cisza);
	bool czy_remis(bool cisza);
	int algorytm_minimax(char gracz);
	int ruch_komputer();
	int ruch_gracz();
	void start_gry();
};
