#include "StdAfx.h"
#include "kolkokrzyzyk.h"

using namespace std;

kolkokrzyzyk::kolkokrzyzyk(void)
{
	for(int i = 1; i <= 9; i++) t[i] = ' ';
	gracz = 'O';
	wybor = 'N';
	licznik=0;
}

void kolkokrzyzyk::wyzeruj_plansze()
{
	for(int i = 1; i <= 9; i++) t[i] = ' ';
}

void kolkokrzyzyk::rysuj_plansze()
{
	for(int i = 1; i <= 9; i++)
	{

		cout << " " << t[i] << " ";
		if(i % 3) cout << "|";
		else if(i != 9) cout << "\n---+---+---\n";
		else cout << endl;
	}
}

bool kolkokrzyzyk::czy_wygrana(char g, bool cisza)
{

	bool sprawdz;
	int i;

	sprawdz = false;

	// sprawdzenie warunkow na wygranie gry

	for(i = 1; i <= 7; i += 3) {
            sprawdz |= ((t[i] == g) && (t[i+1] == g) && (t[i+2] == g));}

	for(i = 1; i <= 3; i++) {
            sprawdz |= ((t[i] == g) && (t[i+3] == g) && (t[i+6] == g));}

	sprawdz |= ((t[1] == g) && (t[5] == g) && (t[9] == g));

	sprawdz |= ((t[3] == g) && (t[5] == g) && (t[7] == g));

	if(sprawdz)
	{
		if(!cisza)
		{
			rysuj_plansze();
			if(g == 'O')
				cout << "\nGratulacje ! Udalo Ci sie wygrac z komputerem !\n\n";
			else
				cout << "\nNiestety, ale komputer okazal sie lepszy :(\n\n";
		}
		return true;
	}
	return false;
}

bool kolkokrzyzyk::czy_remis(bool cisza)
{

	// Je�li napotkamy spacj�, to plansza posiada wolne pola - zwracamy false
	for(int i = 1; i <= 9; i++) if(t[i] == ' ') return false;

	// Jesli nie posiada wolnych pol, to zwracamy true - remis

	if(!cisza)
	{
		rysuj_plansze();
		cout << "\nGra zakonczona remisem !\n\n";
	}
	return true;
}

int kolkokrzyzyk::algorytm_minimax(char gracz)
{
	int m, mmx;

	if(czy_wygrana(gracz,true)) return (gracz == 'X') ? 1 : -1;

	if(czy_remis(true)) return 0;

	gracz = (gracz == 'X') ? 'O' : 'X';

	mmx = (gracz == 'O') ? 10 : -10;

	for(int i = 1; i <= 9; i++)
	if(t[i] == ' ')
	{
		t[i] = gracz;
		m = algorytm_minimax(gracz);
		t[i] = ' ';
		if(((gracz == 'O') && (m < mmx)) || ((gracz == 'X') && (m > mmx))) mmx = m;
	}
	return mmx;
}

int kolkokrzyzyk::ruch_komputer()
{
	int ruch, i, m, mmx;

	 mmx = -10;
	 for(i = 1; i <= 9; i++)
		if(t[i] == ' ')
		{
			t[i] = 'X';
			m = algorytm_minimax(gracz);
			t[i] = ' ';
			if(m > mmx)
			{
				mmx = m; ruch = i;
			}
		}
		licznik=licznik+1;
		if(licznik==9){licznik=0;}
	return ruch;
	//}

}

int kolkokrzyzyk::ruch_gracz()
{
	int r;
    cout << "!***********! "<<endl;
	rysuj_plansze();
	cout << "!***********! "<<endl;
	if(gracz == 'O')
	{
		cout << "\nWybierz swoje pole [1 - 9] : ";
		cin >> r;
	}
	else
	{
		r = ruch_komputer();
		cout << "\nKomputer wybral pole nr : " << r << endl;
	}
	cout << "\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n\n";
	if((r >= 1) && (r <= 9) && (t[r] == ' '))
	{
		t[r] = gracz;
		gracz = (gracz == 'O') ? 'X' : 'O';
		return gracz;
	}
	else
	{
		cout << "Uwaga ! Wybrane pole zostalo ju� wybrane lub nie istnieje !" << endl << endl;
		ruch_gracz();
	}
}

void kolkokrzyzyk::start_gry()
{
	do
	{
		cout << "* * * * * * * * * * * * * * * * * \n\n"
				"* * * Gra w Kolko i Krzyzyk * * * \n\n"
				"* * * * * * * * * * * * * * * * * \n\n\n";
		wyzeruj_plansze();
		gracz = 'O';

		while(!czy_wygrana('X',false) && !czy_wygrana('O',false) && !czy_remis(false)) ruch_gracz();

		cout << "Rozpoczac nowa gre ? (T = TAK) : ";
		cin >> wybor;
		cout << "\n\n\n";

	} while((wybor == 'T') || (wybor == 't'));
}
